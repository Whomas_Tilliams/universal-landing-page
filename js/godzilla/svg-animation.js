var SvgAnimator = (function() {

	var $leadSection = $('.section--lead');

	var init = function(selector, delay){

		setTimeout(function(){

			$(selector).each(function(){

				var $svg = $(this);

				animateSvg($svg)

			});

		}, delay);


	}

    var easeOutQuad = function(currentIteration, startValue, changeInValue, totalIterations) {
        return -changeInValue * (currentIteration /= totalIterations) * (currentIteration - 2) + startValue;
    }

    var onPathDrawComplete = function(){
    	$leadSection.addClass('section--reveal');
    }

    var animateSvg = function($svg) {


            var points = [];
            var nextPoint = 0;
            var endPoint = ($svg.data('end') / 100) || 1;

            var pins = $svg.find('.godzilla-pin');

            for (var i = 0; i < $svg.data('points').length; i++) {

                points.push({
                    percent: $svg.data('points')[i] / 100,
                    triggered: false,
                    node: pins[i] || document.createElement('div')
                });


            }

            $svg.find('.godzilla-svg__fill path').each(function() {

                var current_frame = 0;
                var total_frames = 960;
                var path = this;
                var length = path.getTotalLength();
                var handle = 0;
                var easingValue;

                path.style.strokeDasharray = length + ' ' + length;
                path.style.strokeDashoffset = length;

                var draw = function() {

                    var percent = current_frame / total_frames;


                    if (percent >= endPoint) {

                        window.cancelAnimationFrame(handle);

                        onPathDrawComplete();
                    } else {

                        current_frame++;

                        easingValue = easeOutQuad(current_frame, 0, 1, total_frames);

                        path.style.strokeDashoffset = Math.floor(length * (1 - easingValue));


                        if (nextPoint < points.length && percent > points[nextPoint].percent) {

                            console.log('triggered');


                            $(points[nextPoint].node).attr('class', 'godzilla-pin godzilla-pin--active');
                            points[nextPoint].triggered = true;

                            nextPoint++;

                        }

                        handle = window.requestAnimationFrame(draw);
                    }

                };

                draw();


            });
    
    }

    return {
    	init: init
    }


})();


SvgAnimator.init('.godzilla-svg', 1200);