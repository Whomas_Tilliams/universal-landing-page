$(document).ready(function() {

    $(".gz-slider").slick({
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 1500,
        adaptiveHeight: true
    });


    $('button.navbar-toggle').on('click', function() {
        $('.gz-nav--mobile').toggleClass('gz-nav--show');
    })


    function godzillaPageChange(index, nextIndex, direction){
        
        var $nextSection = $('.section').eq(nextIndex - 1);

        if ($nextSection.data('bg') === 'light') {

            $('.gz-nav--desktop').removeClass('dark-bg').addClass('light-bg');

        } 
        
        else {
            
            $('.gz-nav--desktop').removeClass('light-bg').addClass('dark-bg');
        
        }
    }

    if (Godzilla.settings.pageType === 'pagepiling') {

        $('#content').pagepiling({
            menu: '#menu',
            anchors: ['page1', 'page2', 'page3', 'page4'],
            navigation: false,

            onLeave: function(index, nextIndex, direction) {
                godzillaPageChange(index, nextIndex, direction)
            }
        });

    }

    else if (Godzilla.settings.pageType === 'fullpage') {

        $('#content').fullpage({
            anchors: ['page1', 'page2', 'page3', 'page4'],
            onLeave: function(index, nextIndex, direction) {
                godzillaPageChange(index, nextIndex, direction)
            }
        });
    }


});
